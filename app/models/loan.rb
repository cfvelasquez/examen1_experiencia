class Loan < ApplicationRecord
  has_many :payments
  validates :customer_name, presence: true
  validates_numericality_of :loan_amount, :greater_than => 5000

  def self.total_Loans
  	Loan.count
  end

  def self.total_Loaned
  	Loan.sum(:loan_amount)
  end

end
