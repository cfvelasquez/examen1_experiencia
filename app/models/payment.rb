class Payment < ApplicationRecord
  belongs_to :loan
  default_scope -> { order(created_at: :desc) }
  validates :loan_id, presence: true
  validates_numericality_of :payment_amount, :greater_than => 0
end
