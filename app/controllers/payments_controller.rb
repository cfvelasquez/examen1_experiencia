class PaymentsController < ApplicationController
	def new
		@payment = Payment.new
	end

	def create
		@payment = Payment.new(payment_params)
		if @payment.save
			flash[:success] = "Payment processed!"
			#redirect_to Loan.find(params[:id])
		else
			render 'new'
		end
	end

	def destroy
	end

	private
		def payment_params
			params.require(:payment).permit(:payment_amount)
		end

end
