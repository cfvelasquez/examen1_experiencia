class LoansController < ApplicationController
  def index
    @loans = Loan.paginate(page: params[:page])
  end

  def show
    @loan = Loan.find(params[:id])
    @payments = @loan.payments.paginate(page: params[:page])
  end

  def new
  	@loan = Loan.new
  end

  def create
  	@loan = Loan.new(loan_params)
  	if @loan.save
  		redirect_to @loan
  	else
  		render 'new'
  	end
  end

  private
  	def loan_params
  		params.require(:loan).permit(:customer_name, :loan_amount, :loan_type)
  	end

end
