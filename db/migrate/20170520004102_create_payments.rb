class CreatePayments < ActiveRecord::Migration[5.0]
  def change
    create_table :payments do |t|
      t.float :payment_amount
      t.references :loan, foreign_key: true

      t.timestamps
    end
    add_index :payments, [:loan_id, :created_at]
  end
end
