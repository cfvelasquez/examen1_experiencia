Rails.application.routes.draw do
  root 'static_pages#home'
  get '/loans', to: 'loans#index'
  get '/payment', to: 'payments#new'
  get '/loans/new', to: 'loans#new'

  resources :loans,		only: [:index, :new, :create, :show]
  resources :payments,	only: [:new, :create, :destroy]
end
